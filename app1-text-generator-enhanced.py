import random

vowels = 'aeiouy'
consonants = 'bcdfghjklmnpqrstvwxz'
letters = vowels+consonants

def name_generator(selected_letters):
	"""
		This function will generate a name using a set of selected letters.
		The length will be equal to the selected_letters list
	"""
	name = []
	for letter in selected_letters:
		if letter == 'v':
			name.append(random.choice(vowels))
		elif letter == 'c':
			name.append(random.choice(consonants))
		elif letter == 'l':
			name.append(random.choice(letters))
		else:
			name.append(letter)

	return ''.join(name)

"""
Main code
"""

#Gets the length for names dynamically
while True:
	try:
		name_length = int(input("What is length of the name (min 3)?"))
		if name_length >= 3:
			break
		raise ValueError
	except ValueError:
		print("Oops!  That was no valid number.  Try again...")

letter_options = []

# Read what kind of letter the name position will have
for i in range(name_length):
	letter_input = input("What letter(" + str(i + 1) + ") do you want? Enter 'v' for vowels, 'c' for consonants, 'l' for any letter: ")
	letter_options.append(letter_input)

# Print 10 generated names
for i in range(10):
	print(name_generator(letter_options))
