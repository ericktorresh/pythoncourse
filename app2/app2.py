import folium, pandas

# Load data
data_frame = pandas.read_csv('Volcanoes-USA.txt')

# Calculate static data
center_location = (data_frame['LAT'].mean(), data_frame['LON'].mean())
minimum_elevation = int(min(data_frame['ELEV']))
maximun_elevation = max(data_frame['ELEV'])
step = int((maximun_elevation - minimum_elevation) / 3)

# Create Map object
map = folium.Map(location = center_location, zoom_start = 6, tiles = 'Mapbox bright')

"""
Part 1: Append markers to the Map
"""

# Color Selector according to the elevation
choose_marker_color = lambda elev : (
	'green' if elev in range(minimum_elevation, minimum_elevation + step) else
	'orange' if elev in range(minimum_elevation + step, minimum_elevation + step * 2) else
	'red'
)

# Create a custom group
map_volcanoes_features_group = folium.FeatureGroup(name="Volcano Locations")

# Append markers to the Map
for lat, lon, name, elev in zip(data_frame['LAT'], data_frame['LON'], data_frame['NAME'], data_frame['ELEV']):
	popup = folium.Popup(name)
	icon = folium.Icon(color=choose_marker_color(elev))
	map_volcanoes_features_group.add_child(folium.Marker(location=[lat,lon],popup=popup,icon=icon))

# Add group of markers
map.add_child(map_volcanoes_features_group)

"""
Part 2: Append GEO JSON data into the Map
"""

# Style selector for geojson data
choose_style_color_geo_json = lambda x: ({
	'fillColor': (
		'green' if x['properties']['POP2005'] <= 10000000 else
		'orange' if 10000000 < x['properties']['POP2005'] < 20000000 else
		'red'
	)
})

# Add geoJson data
map.add_child(folium.GeoJson(
	data=open('world-population.json'),
	name="Unemployment",
	style_function=choose_style_color_geo_json
))

map.add_child(folium.LayerControl())

# Save into a file
map.save(outfile='app3_output.html')



