import math

def convert_celsius_to_fahrenheit(degrees):
	return degrees * 9/5 + 32 if degrees >= -273.15 else float('NaN')

#Main section
temperatures_in_celsius=[10, -20, -289, 100]

file = open('exercise5-output', 'w+')

for temperature in temperatures_in_celsius:
	fahrenheit = convert_celsius_to_fahrenheit(temperature)
	if math.isnan(fahrenheit) == False:
		file.write(str(fahrenheit) + '\n')

file.close()
