import glob
import datetime

inputfiles = glob.glob('./exercise6-input/*.txt')
merged_content = []

for file in inputfiles:
	with open(file) as opened_file:
		merged_content.append(opened_file.read())
		opened_file.close()

filename = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S-%f') + '.txt'

output_file = open(filename, 'w+')

output_file.write('\n'.join(merged_content))

output_file.close()
