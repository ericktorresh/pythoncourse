import tkinter as tk
from tkinter import messagebox

class Application(tk.Frame):
	def __init__(self, master=None):
		super().__init__(master)
		self.pack(anchor=tk.CENTER, expand=1)
		self.master.title('Weight Converter')
		self.entry_input_value = tk.StringVar()
		self.create_widgets()

	def calculate_weights(self):
		try:
			self.grams_text.delete(1.0, tk.END)
			self.pounds_text.delete(1.0, tk.END)
			self.ounces_text.delete(1.0, tk.END)

			kg = float(self.entry_input_value.get())

			self.grams_text.insert(tk.END, kg * 1000)
			self.pounds_text.insert(tk.END, kg * 2.20462)
			self.ounces_text.insert(tk.END, kg * 35.274)

		except ValueError:
			messagebox.messagebox.showerror('Error',  'Oops!  That was no valid number.  Try again...')

	def create_widgets(self):
		self.label = tk.Label(self, text='Kg')
		self.label.grid(row=1, column=0)

		self.entry_input = tk.Entry(self, textvariable=self.entry_input_value)
		self.entry_input.grid(row=1, column=1)

		self.convert_button = tk.Button(self, text='Convert', command=self.calculate_weights)
		self.convert_button.grid(row=1, column=2)

		# Label and Text for Grams
		self.grams_label = tk.Label(self, text='Grams')
		self.grams_label.grid(row=2, column=0)
		self.grams_text = tk.Text(self, width=20, height=1)
		self.grams_text.grid(row=2, column=1)

		# Label and Text for pounds
		self.pounds_label = tk.Label(self, text='Pounds')
		self.pounds_label.grid(row=3, column=0)
		self.pounds_text = tk.Text(self, width=20, height=1)
		self.pounds_text.grid(row=3, column=1)

		# Label and Text for ounces
		self.ounces_label = tk.Label(self, text='Ounces')
		self.ounces_label.grid(row=4, column=0)
		self.ounces_text = tk.Text(self, width=20, height=1)
		self.ounces_text.grid(row=4, column=1)

app = Application()
app.mainloop()